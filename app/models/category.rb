class Category < ActiveRecord::Base
  include ValidateNameUniquenessAcrossModels
  include SendEmailOnCreation
  has_many :courses, dependent: :destroy
  belongs_to :vertical
  enum state: [:active, :inactive]
end
