class Course < ActiveRecord::Base
  include SendEmailOnCreation
  belongs_to :category
  enum state: [:active, :inactive]
end
