class Vertical < ActiveRecord::Base
  include ValidateNameUniquenessAcrossModels
  include SendEmailOnCreation

  has_many :categories, dependent: :destroy
  enum state: [:active, :inactive]
end
