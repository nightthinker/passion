module SendEmailOnCreation
  extend ActiveSupport::Concern

  included do
    after_create :send_email
  end

  private

  def send_email
    subject = "New #{self.class.to_s} created with id #{self.id} and name #{self.name}"
    begin
      Createmailer.create_entity_email(subject).deliver_later
    rescue
      #print it in console and fail silently
      puts subject
      #Fail silently as heroku has some issues while sending email
      #Errno::ECONNREFUSED: Connection refused - connect(2) for "localhost" port 25
    end
  end

end
