module ValidateNameUniquenessAcrossModels
  extend ActiveSupport::Concern

  @@participants = []

  included do
    @@participants << self
    validate :unique_name
  end

  private

  def unique_name
    if self.name.blank?
      self.errors.add :name, 'can not be blank'
    else
      @@participants.each do |model|
        scope = model.where(name: self.name)
        if self.persisted? && model == self.class
          scope = scope.where.not(id: self.id)
        end
        if scope.any?
          self.errors.add :name, 'is invalid'
          break
        end
      end
    end
  end

end
