class BaseSerializer < ActiveModel::Serializer
  attributes :id, :name, :state
end
