module V1
  module Api
    class CategoriesController < ApplicationController
      before_action :set_category, only: [:show, :update, :destroy, :courses]

      def index
        respond_with Category.all
      end

       def courses
        render json: {courses: @category.courses}
      end

      def show
        respond_with @category
      end

      def create
        begin
          c = Category.create!(category_params)
          respond_with :v1, :api, c
        rescue => error
          respond_with_error(error.message)
        end
      end

      def update
        begin
          @category.update!(category_params)
          render json: @category
        rescue => error
          respond_with_error(error.message)
        end
      end

      def destroy
        begin
          @category.destroy
          respond_with_success("Category with id: #{@category.id} deleted successfully.")
        rescue => error
         respond_with_error(error.message)
        end
      end

      private

      def set_category
        begin
          @category = Category.find(params[:id])
        rescue => error
          respond_with_error(error.message)
        end
      end

      def category_params
        params[:category].permit(:name, :state, :vertical_id)
      end

    end
  end
end
