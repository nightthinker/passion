module V1
  module Api
    class CoursesController < ApplicationController
      before_action :set_course, only: [:show, :update, :destroy]

      def index
        respond_with Course.all
      end

      def show
        respond_with @course
      end

      def create
        begin
          c = Course.create!(course_params)
          respond_with :v1, :api, c
        rescue => error
          respond_with_error(error.message)
        end
      end

      def update
        begin
          @course.update!(course_params)
          render json: @course
        rescue => error
          respond_with_error(error.message)
        end
      end

      def destroy
        begin
          @course.destroy
          respond_with_success("Course with id: #{@course.id} deleted successfully.")
        rescue => error
         respond_with_error(error.message)
        end
      end

      private

      def set_course
        begin
          @course = Course.find(params[:id])
        rescue => error
          respond_with_error(error.message)
        end
      end

      def course_params
        params[:course].permit(:name, :state, :category_id)
      end

    end
  end
end
