module V1
  module Api
    class VerticalsController < ApplicationController
      before_action :set_vertical, only: [:show, :update, :destroy, :categories]

      def index
        respond_with Vertical.all
      end

      def categories
        render json: {categories: @vertical.categories}
      end

      def show
        respond_with @vertical
      end

      def create
        begin
          v = Vertical.create!(vertical_params)
          respond_with :v1, :api, v
        rescue => error
          respond_with_error(error.message)
        end

      end

      def update
        begin
          @vertical.update!(vertical_params)
          render json: @vertical
        rescue => error
          respond_with_error(error.message)
        end
      end

      def destroy
        begin
          @vertical.destroy
          respond_with_success("Vertical with id: #{@vertical.id} deleted successfully.")
        rescue => error
         respond_with_error(error.message)
        end
      end

      private

      def set_vertical
        begin
          @vertical = Vertical.find(params[:id])
        rescue => error
          respond_with_error(error.message)
        end
      end

      def vertical_params
        params[:vertical].permit(:name, :state)
      end

    end
  end
end
