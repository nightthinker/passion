class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  #To make POST/PUT/PATCH/DEL api work without auth for now
  skip_before_action :verify_authenticity_token
  respond_to :json


  #To be used by all controllers for error responses for API
  def respond_with_error(message)
    render json: {status: 'error', message: message}
  end

  def respond_with_success(message)
    render json: {status: 'success', message: message}
  end
end
