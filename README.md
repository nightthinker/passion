This is a repo for passion.io challenge. (https://github.com/independenc3/passion4coding/blob/master/readme.md#task-at-hand---only-backend--full-stack-positions)

Live at: http://passion-code.herokuapp.com
Sample API: http://passion-code.herokuapp.com/v1/api/verticals


**Relevant APIs**


1. GET /v1/api/verticals => Get all verticals
2. GET /v1/api/verticals/:id/categories => Get categories for vertical with identifier :id
3. POST /v1/api/verticals/ => Create vertical
4. PUT /v1/api/verticals/:id =>  Update vertical with identifier :id
5. DEL /v1/api/verticals/:id => Delete vertical with identifier :id

6. GET /v1/api/categories => Get all categories
7. GET /v1/api/categories/:id/courses => Get courses for category with identifier :id
8. POST /v1/api/categories/ => Create category
9. PUT /v1/api/categories/:id =>  Update category with identifier :id
10. DEL /v1/api/categories/:id => Delete category with identifier :id

11. GET /v1/api/courses => Get all courses
12. POST /v1/api/courses/ => Create course
13. PUT /v1/api/courses/:id =>  Update courses with identifier :id
14. DEL /v1/api/courses/:id => Delete courses with identifier :id


**Objects Signature:**

1. vertical: {name: String, state: String}
2. category: {name: String, vertical_id: Integer, state: String}
3. course  : {name: String, category_id: Integer, state: String}

**JSON Signaure:**

1. Verticals: https://api.myjson.com/bins/11qdcf
2. Categories: https://api.myjson.com/bins/rm027
3. Courses: https://api.myjson.com/bins/15ayz3



**Solution/Approach:**

1. Three models and controllers namely: Vertical, Category and Course
2. Postgres database for production and sqlite for development
3. API versioning with V1
4. Basic CRUD for all models and nested api for getting all categories for a single vertical and all courses for a single category.
5. Gmail smtp setup for sending email
6. Activeadmin to view/filter/create all data using UI (PS: It's not working on heroku as of now, but works locally)
7. Rspec to test creation of all models
8. Corresponding errors to be returned in APIs in case of failure
9. Faker gem to generate seed data

Bonus
1. TODO: OAuth: We can use doorkeeper gem earlier to make an application oauth provider. I have used it before and it may take a day or two to implement and fully test it.

**Reusablity:**


1. Uniqueness of name across vertical and category using ActiveSupport::Concern (https://bitbucket.org/nightthinker/passion/src/master/app/models/concerns/validate_name_uniqueness_across_models.rb)
2. Send email on creation of vertical, category or course using ActiveSupport::Concern (https://bitbucket.org/nightthinker/passion/src/master/app/models/concerns/send_email_on_creation.rb)
3. Error/Success response for APIs at common place (https://bitbucket.org/nightthinker/passion/src/85839c2daaa8d8328408695d1135d2af0f5e2f13/app/controllers/application_controller.rb#lines-11)
4. Base serializer for models (https://bitbucket.org/nightthinker/passion/src/master/app/serializers/base_serializer.rb)


**Questions**


*How does your solution perform?*

Solution is well tested and will perform well as per channelenge over single machine and single database.

*How does your solution scale?*

To scale this solution, we can do following


1. Add sidekiq to send email as a job
2. Data shrading in case data is huge
3. Separating write database and read database
4. Add a cache layer to serve data (elasticsearch maybe)

*What would you improve next?*


1. Standardize the API response in {success, messsage and data} format
2. Add Oauth or jwt authentication with devise
3. Add a cache layer to serve unchanged data


Best,
Ankur
