require 'rails_helper'

RSpec.describe Course, type: :model do
  context "Create Course" do
    before :each do
      v = Vertical.create(name: Faker::Lorem.word.capitalize)
      @cat = Category.create(name: Faker::Lorem.word.capitalize, vertical: v)
    end
    it "create course successfully" do
      course = Course.new(name: Faker::Lorem.word.capitalize, category: @cat)
      expect(course.save!).to be_truthy
    end

    it "create course successfully with same name as category" do
      course = Course.new(name: @cat.name, category: @cat)
      expect(course.save!).to be_truthy
    end
    it "send email on creation of course" do
      course = Course.new(name: Faker::Lorem.word.capitalize, category: @cat)
      allow_any_instance_of(Createmailer).to receive(:create_entity_email).and_return(true)
      course.save!
    end
  end
end
