require 'rails_helper'

RSpec.describe Category, type: :model do
  context "Create category" do
    before :each do
      @v = Vertical.create(name: Faker::Lorem.word.capitalize)
    end
    it "create cagegory successfully" do
      cat = Category.new(name: Faker::Lorem.word.capitalize, vertical: @v)
      expect(cat.save!).to be_truthy
    end

    it "fails creating cagegory with same name as vertical" do
      cat = Category.new(name: @v.name, vertical: @v)
      expect{cat.save!}.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Name is invalid')
    end
    it "send email on creation of category" do
      cat = Category.new(name: Faker::Lorem.word.capitalize, vertical: @v)
      allow_any_instance_of(Createmailer).to receive(:create_entity_email).and_return(true)
      cat.save!
    end
  end
end
