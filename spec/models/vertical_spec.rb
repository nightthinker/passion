require 'rails_helper'

RSpec.describe Vertical, type: :model do
  context "Create Vertical" do

    it "create vertical successfully" do
      vertical = Vertical.new(name: Faker::Lorem.word.capitalize)
      expect(vertical.save!).to be_truthy
    end

    it "creation of vertical fails with same name as category" do
      v = Vertical.create!(name: Faker::Lorem.word.capitalize)
      cat = Category.create(name: Faker::Lorem.word.capitalize, vertical: v)
      v1 = Vertical.new(name: cat.name)
      expect{v1.save!}.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Name is invalid')
    end

    it "send email on creation of vertical" do
      vertical = Vertical.new(name: Faker::Lorem.word.capitalize)
      allow_any_instance_of(Createmailer).to receive(:create_entity_email).and_return(true)
      vertical.save!
    end
  end
end
