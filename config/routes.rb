Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :v1 do
    namespace :api, defaults: {format: 'json'} do
      resources :verticals do
        member do
          get 'categories'
        end
      end
      resources :categories do
        member do
          get 'courses'
        end
      end
      resources :courses
    end
  end
end
