class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :state, default: 0
      t.integer :vertical_id, null: false

      t.timestamps null: false
    end
  end
end
