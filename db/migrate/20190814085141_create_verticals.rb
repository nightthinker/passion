class CreateVerticals < ActiveRecord::Migration
  def change
    create_table :verticals do |t|
      t.string :name
      t.integer :state, default: 0

      t.timestamps null: false
    end
  end
end
