class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.integer :state, default: 0
      t.integer :category_id, null: false

      t.timestamps null: false
    end
  end
end
