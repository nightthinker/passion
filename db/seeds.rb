# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

3.times do
  Vertical.create(name: Faker::Lorem.word.capitalize)
end

6.times do
  Category.create(
    name: Faker::Lorem.word.capitalize,
    vertical: Vertical.order('RANDOM()').first
  )
end

9.times do
  Course.create(
    name: Faker::Lorem.word.capitalize,
    category: Category.order('RANDOM()').first
  )
end

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
